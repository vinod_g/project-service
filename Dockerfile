FROM tomcat:9-jre8
COPY ./target/project-service.war /usr/local/tomcat/webapps/ROOT.war
COPY ./target/project-service /usr/local/tomcat/webapps/ROOT
EXPOSE 8080
