package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.model.Project;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.repository.ProjectRepository;
import com.vinodg.projectservice.repository.TaskRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceTest {

    @MockBean
    private ProjectRepository repository;
    @MockBean
    private TaskRepository taskRepository;

    @Autowired
    private ProjectService projectService;

    private Project project;
    private ProjectDTO projectDTO;
    private Task task;

    @Before
    public void setup(){

        this.project = new Project("1", "Project1", "2019-01-05", "2019-01-06", 0, 1);
        this.projectDTO = new ProjectDTO("1", "Project1", "2019-01-05", "2019-01-06", 0, 1,0,0);
        this.task = new Task("1", null, null, "Task1", null,null,0);
    }

    @Test
    public void fetchProjects() {
        when(repository.findAll()).thenReturn(Collections.singletonList(project));
        when(taskRepository.findByProjectId("1")).thenReturn(Collections.singletonList(this.task));
        List<ProjectDTO> allProjects = projectService.fetchProjects();
        assertThat(allProjects.size(), is(1));
        assertThat(allProjects.get(0).getProject(), is("Project1"));
        assertThat(allProjects.get(0).getCountOfTasks(), is(1));
        assertThat(allProjects.get(0).getCountOfCompletedTasks(), is(0));
    }

    @Test
    public void fetchProjectById() {
        when(repository.findById("1")).thenReturn(Optional.of(this.project));
        ProjectDTO response = projectService.fetchProjectById("1");
        assertThat(response.getProject(), is("Project1"));
    }

    @Test
    public void fetchProjectById_NoFound() {
        when(repository.findById("1")).thenReturn(Optional.empty());
        ProjectDTO response = projectService.fetchProjectById("1");
        assertThat(response, is(nullValue()));
    }

    @Test
    public void updateProject() {
        projectService.updateProject(this.projectDTO);
        verify(repository).save(any(Project.class));
    }

    @Test
    public void createProject() {
        projectService.createProject(this.projectDTO);
        verify(repository).save(any(Project.class));
    }

    @Test
    public void deleteProjectById() {
        projectService.deleteProjectById("1");
        verify(repository).deleteById("1");
    }
}
