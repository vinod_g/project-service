package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.model.ParentTask;
import com.vinodg.projectservice.model.ParentTask;
import com.vinodg.projectservice.repository.ParentTaskRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParentTaskServiceTest {

    @MockBean
    private ParentTaskRepository repository;

    @Autowired
    private ParentTaskService parentTaskService;

    private ParentTask parentTask;
    private ParentTaskDTO parentTaskDTO;

    @Before
    public void setup(){
        this.parentTask = new ParentTask("1",  "Parent Task1");
        this.parentTaskDTO = new ParentTaskDTO("1",  "Parent Task1");
    }

    @Test
    public void fetchAll() {
        when(repository.findAll()).thenReturn(Collections.singletonList(parentTask));
        List<ParentTaskDTO> allTasks = parentTaskService.fetchParentTasks();
        assertThat(allTasks.size(), is(1));
        assertThat(allTasks.get(0).getParentTask(), is("Parent Task1"));
    }

    @Test
    public void updateParentTask() {
        parentTaskService.updateParentTask(this.parentTaskDTO);
        verify(repository).save(any(ParentTask.class));
    }

    @Test
    public void createParentTask() {
        parentTaskService.createParentTask(this.parentTaskDTO);
        verify(repository).save(any(ParentTask.class));
    }

    @Test
    public void deleteParentTaskById() {
        parentTaskService.deleteParentTaskById("1");
        verify(repository).deleteById("1");
    }
}
