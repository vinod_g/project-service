package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.TaskDTO;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.model.User;
import com.vinodg.projectservice.repository.TaskRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskServiceTest {

    @MockBean
    private TaskRepository repository;

    @Autowired
    private TaskService taskService;

    private Task task;
    private TaskDTO taskDTO;

    @Before
    public void setup(){
        this.task = new Task("1", null, null, "Task1", null,null,0);
        this.taskDTO = new TaskDTO("1", null, null, "Task1", null,null,0, null);
    }

    @Test
    public void fetchTasks() {
        when(repository.findAll()).thenReturn(Collections.singletonList(task));
        List<TaskDTO> allTasks = taskService.fetchTasks();
        assertThat(allTasks.size(), is(1));
        assertThat(allTasks.get(0).getTask(), is("Task1"));
    }

    @Test
    public void fetchTaskById() {
        when(repository.findById("1")).thenReturn(Optional.of(this.task));
        TaskDTO response = taskService.fetchTaskById("1");
        assertThat(response.getTask(), is("Task1"));
    }

    @Test
    public void fetchTaskById_NoFound() {
        when(repository.findById("1")).thenReturn(Optional.empty());
        TaskDTO response = taskService.fetchTaskById("1");
        assertThat(response, is(nullValue()));
    }

    @Test
    public void updateTask() {
        taskService.updateTask(this.taskDTO);
        verify(repository).save(any(Task.class));
    }

    @Test
    public void createTask() {
        taskService.createTask(this.taskDTO);
        verify(repository).save(any(Task.class));
    }

    @Test
    public void deleteTaskById() {
        taskService.deleteTaskById("1");
        verify(repository).deleteById("1");
    }
}
