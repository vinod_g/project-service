package com.vinodg.projectservice.service;


import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.model.User;
import com.vinodg.projectservice.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @MockBean
    private UserRepository repository;

    @Autowired
    private UserService userService;


    private User user;

    private UserDTO userDTO;

    @Before
    public void setup(){
        this.user = new User("1", "John","Smith" ,null, null, null);
        this.userDTO = new UserDTO("1", "John","Smith" ,null, null, null);
    }

    @Test
    public void fetchUsers() {
        when(repository.findAll()).thenReturn(Collections.singletonList(user));
        List<UserDTO> allUsers = userService.fetchUsers();
        assertThat(allUsers.size(), is(1));
        assertThat(allUsers.get(0).getFirstName(), is("John"));
    }

    @Test
    public void fetchUserById() {
        when(repository.findById("1")).thenReturn(Optional.of(this.user));
        UserDTO response = userService.fetchUserById("1");
        assertThat(response.getFirstName(), is("John"));
    }

    @Test
    public void fetchUserById_NoFound() {
        when(repository.findById("1")).thenReturn(Optional.empty());
        UserDTO response = userService.fetchUserById("1");
        assertThat(response, is(nullValue()));
    }

    @Test
    public void updateUser() {
        userService.updateUser(this.userDTO);
        verify(repository).save(any(User.class));
    }

    @Test
    public void createUser() {
        userService.createUser(this.userDTO);
        verify(repository).save(any(User.class));
    }

    @Test
    public void deleteUserById() {
        userService.deleteUserById("1");
        verify(repository).deleteById("1");
    }
    @Test
    public void fetchUserByTaskId() {
        when(repository.findByTaskId("1")).thenReturn(this.user);
        UserDTO response = userService.fetchUserByTaskId("1");
        assertThat(response.getFirstName(), is("John"));
    }
}
