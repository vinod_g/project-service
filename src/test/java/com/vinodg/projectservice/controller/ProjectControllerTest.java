package com.vinodg.projectservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.model.Project;
import com.vinodg.projectservice.service.ProjectService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProjectControllerTest {

    public static final String API_URI = "/api/projects";
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ProjectService projectService;
    @Autowired
    ObjectMapper objectMapper;

    private ProjectDTO projectDTO;

    @Before
    public void setup() {
        this.projectDTO = new ProjectDTO("1", "Project1",  null, null, 0,1,0,0);
    }
    @Test
    public void getAllProjects() throws Exception {
        when(projectService.fetchProjects()).thenReturn(Collections.singletonList(projectDTO));
        mvc.perform(get(API_URI)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].project", is(projectDTO.getProject())));
    }

    @Test
    public void getProjectById() throws Exception {
        when(projectService.fetchProjectById("1")).thenReturn(this.projectDTO);
        mvc.perform(get(API_URI + "/1")).andExpect(status().isOk())
                .andExpect(jsonPath("$.project", is(projectDTO.getProject())));
    }

    @Test
    public void getProjectByIdNoFound() throws Exception {
        when(projectService.fetchProjectById("1")).thenReturn(null);
        mvc.perform(get(API_URI + "/1")).andExpect(status().isNotFound());
    }

    @Test
    public void updateProject() throws Exception {
        mvc.perform(put(API_URI + "/1") .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(projectDTO))).andExpect(status().isOk());
    }

    @Test
    public void createProject() throws Exception {
        final ProjectDTO newProject = new ProjectDTO(null, "Project1",  null, null, 0, 1,0,0);
        when(projectService.createProject(Mockito.any())).thenReturn(newProject);
        mvc.perform(post(API_URI).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(projectDTO))).andExpect(status().isOk()).andExpect(jsonPath("$.project", is(projectDTO.getProject())));
    }

    @Test
    public void deleteProject()  throws Exception {
        mvc.perform(delete(API_URI + "/1")).andExpect(status().isOk());
        verify(projectService).deleteProjectById("1");
    }

}
