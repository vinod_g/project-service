package com.vinodg.projectservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.service.ParentTaskService;
import com.vinodg.projectservice.model.ParentTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ParentTaskControllerTest {

    public static final String PARENT_TASKS_URI = "/api/parentTasks";
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ParentTaskService parentTaskService;
    @Autowired
    ObjectMapper objectMapper;

    private ParentTask parentTask;
    private ParentTaskDTO parentTaskDTO;

    @Before
    public void setup(){
        this.parentTask = new ParentTask("1",  "Parent Task1");
        this.parentTaskDTO = new ParentTaskDTO("1",  "Parent Task1");
    }

    @Test
    public void createTask() throws Exception {
        when(parentTaskService.createParentTask(Mockito.any())).thenReturn(parentTaskDTO);
        mvc.perform(post(PARENT_TASKS_URI).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(this.parentTask))).andExpect(status().isOk()).andExpect(jsonPath("$.parentTask", is(this.parentTask.getParentTask())));
    }

    @Test
    public void updateTask() throws Exception {
        mvc.perform(put(PARENT_TASKS_URI + "/1").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(parentTask))).andExpect(status().isOk());

    }

    @Test
    public void fetchAll() throws Exception {
        when(parentTaskService.fetchParentTasks()).thenReturn(Collections.singletonList(parentTaskDTO));
        mvc.perform(get(PARENT_TASKS_URI)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].parentTask", is(parentTask.getParentTask())));
    }

    @Test
    public void deleteTask ()  throws Exception {
        mvc.perform(delete(PARENT_TASKS_URI + "/1")).andExpect(status().isOk());
        verify(parentTaskService).deleteParentTaskById("1");
    }

}
