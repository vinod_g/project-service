package com.vinodg.projectservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.model.User;
import com.vinodg.projectservice.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    public static final String API_URI = "/api/users";
    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserService userService;
    @Autowired
    ObjectMapper objectMapper;

    private UserDTO user;

    @Before
    public void setup() {
        this.user = new UserDTO("1", "John","Smith" ,null, null, null);
    }
    @Test
    public void getAllUsers() throws Exception {
        when(userService.fetchUsers()).thenReturn(Collections.singletonList(user));
        mvc.perform(get(API_URI)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName", is(user.getFirstName())));
    }

    @Test
    public void getUserById() throws Exception {
        when(userService.fetchUserById("1")).thenReturn(this.user);
        mvc.perform(get(API_URI + "/1")).andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(user.getFirstName())));
    }

    @Test
    public void getUserByIdNoFound() throws Exception {
        when(userService.fetchUserById("1")).thenReturn(null);
        mvc.perform(get(API_URI + "/1")).andExpect(status().isNotFound());
    }

    @Test
    public void updateUser() throws Exception {
        mvc.perform(put(API_URI + "/1") .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user))).andExpect(status().isOk());
    }

    @Test
    public void createUser() throws Exception {
        final UserDTO newUser = new UserDTO(null, "John","Smith" ,null, null, null);
        when(userService.createUser(Mockito.any())).thenReturn(newUser);
        mvc.perform(post(API_URI).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user))).andExpect(status().isOk()).andExpect(jsonPath("$.firstName", is(user.getFirstName())));
    }

    @Test
    public void deleteUser()  throws Exception {
        mvc.perform(delete(API_URI + "/1")).andExpect(status().isOk());
        verify(userService).deleteUserById("1");
    }

}
