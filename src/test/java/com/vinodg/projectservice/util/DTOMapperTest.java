package com.vinodg.projectservice.util;

import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.dto.TaskDTO;
import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.model.ParentTask;
import com.vinodg.projectservice.model.Project;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.model.User;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


public class DTOMapperTest {

    private final DTOMapper dtoMapper = new DTOMapper();
    @Test
    public void toUserDTO() {
        User user= new User("1", "John","Smith" ,null, null, null);
        UserDTO userDTO = dtoMapper.toUserDTO(user);
        assertThat(userDTO.getId(),is(equalTo(user.getId())));
    }

    @Test
    public void toTaskDTO() {
        Task task = new Task("1", null, null, "Task 1", null,null,0);
        TaskDTO taskDTO = dtoMapper.toTaskDTO(task);
        assertThat(taskDTO.getId(), is(equalTo(task.getId())));
    }

    @Test
    public void toParentTaskDTO() {
        ParentTask parentTask = new ParentTask("1", "Parent task 1");
        ParentTaskDTO parentTaskDTO = dtoMapper.toParentTaskDTO(parentTask);
        assertThat(parentTaskDTO.getParentId(), is(equalTo(parentTask.getParentId())));
    }

    @Test
    public void toProjectDTO() {
        Project project =new Project("1" , "Project 1" , null, null , 0, 1);
        ProjectDTO projectDTO = dtoMapper.toProjectDTO(project);
        assertThat(projectDTO.getId(), is(equalTo(project.getId())));
    }

    @Test
    public void toUser() {
        UserDTO userDTO = new UserDTO("1", "John","Smith" ,null, null, null);
        User user = dtoMapper.toUser(userDTO);
        assertThat(user.getId(),is(equalTo(userDTO.getId())));
    }

    @Test
    public void toTask() {
        TaskDTO taskDTO = new TaskDTO("1", null, null, "Task 1", null,null,0, null);
        Task task = dtoMapper.toTask(taskDTO);
        assertThat(task.getId(), is(equalTo(task.getId())));
    }

    @Test
    public void toParentTask() {
        ParentTaskDTO parentTaskDTO = new ParentTaskDTO("1", "Parent task 1");
        ParentTask parentTask = dtoMapper.toParentTask(parentTaskDTO);
        assertThat(parentTask.getParentId(), is(equalTo(parentTaskDTO.getParentId())));
    }

    @Test
    public void toProject() {
        ProjectDTO projectDTO = new ProjectDTO("1", "Project 1", null, null, 0, 1, 0, 0);
        Project project = dtoMapper.toProject(projectDTO);
        assertThat(project.getId(), is(equalTo(projectDTO.getId())));
    }

    @Test
    public void toUserDTOs() {
        User user= new User("1", "John","Smith" ,null, null, null);
        List<UserDTO> userDTOs = dtoMapper.toUserDTOs(Collections.singletonList(user));
        assertThat(userDTOs.get(0).getId(),is(equalTo(user.getId())));
    }

    @Test
    public void toParentTaskDTOs() {
        ParentTask parentTask = new ParentTask("1", "Parent task 1");
        List<ParentTaskDTO> parentTaskDTOs = dtoMapper.toParentTaskDTOs(Collections.singletonList(parentTask));
        assertThat(parentTaskDTOs.get(0).getParentId(), is(equalTo(parentTask.getParentId())));
    }

    @Test
    public void toProjectDTOs() {
        Project project =new Project("1" , "Project 1" , null, null , 0, 1);
        List<ProjectDTO> projectDTOs = dtoMapper.toProjectDTOs(Collections.singletonList(project));
        assertThat(projectDTOs.get(0).getId(), is(equalTo(project.getId())));
    }

    @Test
    public void nullObjects() {
        try {
            dtoMapper.toUserDTO(null);
            dtoMapper.toUser(null);
            dtoMapper.toProject(null);
            dtoMapper.toProjectDTO(null);
            dtoMapper.toTask(null);
            dtoMapper.toTaskDTO(null);
            dtoMapper.toParentTask(null);
            dtoMapper.toParentTaskDTO(null);
            dtoMapper.toUserDTOs(null);
        } catch (Exception e) {
            fail();
        }
    }
}