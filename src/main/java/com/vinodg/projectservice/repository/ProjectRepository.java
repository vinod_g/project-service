package com.vinodg.projectservice.repository;

import com.vinodg.projectservice.model.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProjectRepository extends MongoRepository<Project, String> {


}
