package com.vinodg.projectservice.repository;

import com.vinodg.projectservice.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TaskRepository extends MongoRepository<Task, String> {


    @Query("{'project.id': ?0}")
    List<Task> findByProjectId(String projectId);

}
