package com.vinodg.projectservice.repository;

import com.vinodg.projectservice.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, String> {

    @Query("{'task.id': ?0}")
    User findByTaskId(String taskId);

}
