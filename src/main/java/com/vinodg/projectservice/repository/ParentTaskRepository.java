package com.vinodg.projectservice.repository;

import com.vinodg.projectservice.model.ParentTask;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ParentTaskRepository extends MongoRepository<ParentTask, String> {


}
