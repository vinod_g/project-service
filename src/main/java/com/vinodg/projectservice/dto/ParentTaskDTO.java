package com.vinodg.projectservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@SuppressWarnings("squid:S1700")
public class ParentTaskDTO {
    private String parentId;
    private String parentTask;
}
