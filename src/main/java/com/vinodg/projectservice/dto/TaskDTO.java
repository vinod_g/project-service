package com.vinodg.projectservice.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@SuppressWarnings("squid:S1700")
public class TaskDTO {
    private String id;
    private ParentTaskDTO parent;
    private ProjectDTO project;
    private String task;
    private String startDate;
    private String endDate;
    private Integer priority;
    private UserDTO user;
}
