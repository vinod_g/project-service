package com.vinodg.projectservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@SuppressWarnings("squid:S1700")
public class ProjectDTO {
    private String id;
    private String project;
    private String startDate;
    private String endDate;
    private Integer priority;
    private Integer managerId;
    private Integer countOfTasks;
    private Integer countOfCompletedTasks;
}