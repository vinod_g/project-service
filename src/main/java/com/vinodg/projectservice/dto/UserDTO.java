package com.vinodg.projectservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@SuppressWarnings("squid:S1700")
public class UserDTO {

    private String id;
    private String firstName;
    private String lastName;
    private Integer employeeId;
    private TaskDTO task;
    private ProjectDTO project;
}