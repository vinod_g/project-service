package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.model.User;
import com.vinodg.projectservice.repository.UserRepository;
import com.vinodg.projectservice.util.DTOMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;

    @Autowired
    private DTOMapper mapper;
    public List<UserDTO> fetchUsers() {
        return mapper.toUserDTOs(repository.findAll());
    }

    public UserDTO fetchUserById(String id) {
        Optional<User> user = repository.findById(id);
        if (!user.isPresent()) {
            log.info("No User found for id {}" , id);
            return null;
        }
        return mapper.toUserDTO(user.get());
    }
    @Transactional
    public void updateUser(UserDTO user) {
        repository.save(mapper.toUser(user));
    }

    public UserDTO createUser(UserDTO user) {
        return mapper.toUserDTO(repository.save(mapper.toUser(user)));
    }

    @Transactional
    public void deleteUserById(String id) {
        repository.deleteById(id);
    }

    public UserDTO fetchUserByTaskId(String taskId) {
        User user = repository.findByTaskId(taskId);
        if (user == null) {
            log.info("No User found for Task id {}" , taskId);
            return null;
        }
        return mapper.toUserDTO(user);
    }
}


