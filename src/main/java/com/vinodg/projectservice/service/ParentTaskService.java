package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.repository.ParentTaskRepository;
import com.vinodg.projectservice.util.DTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ParentTaskService {

    @Autowired
    private ParentTaskRepository repository;

    @Autowired
    private DTOMapper mapper;
    public List<ParentTaskDTO> fetchParentTasks() {
        return mapper.toParentTaskDTOs(repository.findAll());
    }

    @Transactional
    public void updateParentTask(ParentTaskDTO parentTask) {
        repository.save(mapper.toParentTask(parentTask));
    }

    public ParentTaskDTO createParentTask(ParentTaskDTO parentTask) {
        return mapper.toParentTaskDTO(repository.save(mapper.toParentTask(parentTask)));
    }

    @Transactional
    public void deleteParentTaskById(String id) {
        repository.deleteById(id);
    }
}


