package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.model.Project;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.repository.ProjectRepository;
import com.vinodg.projectservice.repository.TaskRepository;
import com.vinodg.projectservice.util.DTOMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Service
public class ProjectService {

    private static final Logger log = LoggerFactory.getLogger(ProjectService.class);

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd");

    private static final Predicate<Task> IS_COMPLETED = t -> t.getEndDate() != null
            && DateTime.parse(t.getEndDate(), DATE_FORMAT).isBeforeNow();

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private DTOMapper mapper;

    public List<ProjectDTO> fetchProjects() {
        List<ProjectDTO> projectDTOs = mapper.toProjectDTOs(projectRepository.findAll());
        for (ProjectDTO project : projectDTOs) {
            List<Task> tasks = taskRepository.findByProjectId(project.getId());
            project.setCountOfTasks(tasks.size());
            project.setCountOfCompletedTasks((int)tasks.stream().filter(IS_COMPLETED).count());
        }
        return projectDTOs;
    }

    public ProjectDTO fetchProjectById(String id) {
        Optional<Project> project = projectRepository.findById(id);
        if (!project.isPresent()) {
            log.info("No Project found for id {}", id);
            return null;
        }
        return mapper.toProjectDTO(project.get());
    }

    @Transactional
    public void updateProject(ProjectDTO project) {
        projectRepository.save(mapper.toProject(project));
    }

    public ProjectDTO createProject(ProjectDTO project) {
        return mapper.toProjectDTO(projectRepository.save(mapper.toProject(project)));
    }

    @Transactional
    public void deleteProjectById(String id) {
        projectRepository.deleteById(id);
    }
}

