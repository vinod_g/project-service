package com.vinodg.projectservice.service;

import com.vinodg.projectservice.dto.TaskDTO;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.repository.TaskRepository;
import com.vinodg.projectservice.util.DTOMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    private static final Logger log = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    private TaskRepository repository;

    @Autowired
    private DTOMapper mapper;

    public List<TaskDTO> fetchTasks() {
        return mapper.toTaskDTOs(repository.findAll());
    }

    public TaskDTO fetchTaskById(String id) {
        Optional<Task> task = repository.findById(id);
        if (!task.isPresent()) {
            log.info("No Task found for id {}" , id);
            return null;
        }
        return mapper.toTaskDTO(task.get());
    }
    @Transactional
    public void updateTask(TaskDTO task) {
        repository.save(mapper.toTask(task));
    }

    public TaskDTO createTask(TaskDTO task) {
        return mapper.toTaskDTO(repository.save(mapper.toTask(task)));
    }

    @Transactional
    public void deleteTaskById(String id) {
        repository.deleteById(id);
    }
}


