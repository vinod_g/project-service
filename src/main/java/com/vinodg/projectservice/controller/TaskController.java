package com.vinodg.projectservice.controller;

import com.vinodg.projectservice.dto.TaskDTO;
import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.service.TaskService;
import com.vinodg.projectservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    public static final Logger log = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    @GetMapping()
    public ResponseEntity<List<TaskDTO>> getAll() {
        List<TaskDTO> tasks = taskService.fetchTasks();
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TaskDTO> findById(@PathVariable("id") String id) {
        log.info("Getting Task with id {}", id);
        final TaskDTO task = taskService.fetchTaskById(id);
        if (task == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task Not Found");
        }
        task.setUser(userService.fetchUserByTaskId(task.getId()));
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") String id, @Valid @RequestBody TaskDTO task) {
        task.setId(id);
        task.getUser().setTask(task);
        UserDTO userDTO = userService.fetchUserByTaskId(task.getId());
        if (userDTO != null && !userDTO.getId().equals(task.getUser().getId())) {
            userDTO.setTask(null);
            userService.updateUser(userDTO);
        }
        userService.updateUser(task.getUser());
        taskService.updateTask(task);

    }

    @PostMapping()
    public ResponseEntity<TaskDTO> create(@Valid @RequestBody TaskDTO task) {
        log.info("Creating Task : {}", task);
        TaskDTO createdTask = taskService.createTask(task);
        if (task.getUser() != null) {
            task.getUser().setTask(createdTask);
            userService.updateUser(task.getUser());
        }
        return new ResponseEntity<>(createdTask, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable String id) {
        log.info("Deleting Task with id  {}", id);
        taskService.deleteTaskById(id);
    }
}
