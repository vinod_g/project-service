package com.vinodg.projectservice.controller;

import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.service.ParentTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/parentTasks")
public class ParentTaskController {

    public static final Logger log = LoggerFactory.getLogger(ParentTaskController.class);

    @Autowired
    private ParentTaskService service;

    @PostMapping()
    public ResponseEntity<ParentTaskDTO> create(@Valid @RequestBody ParentTaskDTO parentTask) {
        log.info("Creating Parent Task : {}", parentTask);
        return new ResponseEntity<>(service.createParentTask(parentTask), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") String id, @Valid @RequestBody ParentTaskDTO parentTask) {
        parentTask.setParentId(id);
        service.updateParentTask(parentTask);
    }

    @GetMapping()
    public ResponseEntity<List<ParentTaskDTO>> fetchAll() {
        List<ParentTaskDTO> tasks = service.fetchParentTasks();
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable String id) {
        log.info("Deleting Parent Task with id  {}", id);
        service.deleteParentTaskById(id);
    }
}
