package com.vinodg.projectservice.controller;

import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/projects")
public class ProjectController {

    public static final Logger log = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;

    @GetMapping()
    public ResponseEntity<List<ProjectDTO>> getAll() {
        List<ProjectDTO> projects = projectService.fetchProjects();
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<ProjectDTO> findById(@PathVariable("id") String id) {
        log.info("Getting Project with id {}", id);
        final ProjectDTO project = projectService.fetchProjectById(id);
        if (project == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Project Not Found");
        }
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") String id, @Valid @RequestBody ProjectDTO project) {
        project.setId(id);
        projectService.updateProject(project);

    }

    @PostMapping()
    public ResponseEntity<ProjectDTO> create(@Valid @RequestBody ProjectDTO project) {
        log.info("Creating Project : {}", project);
        return new ResponseEntity<>(projectService.createProject(project), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable String id) {
        log.info("Deleting Project with id  {}", id);
        projectService.deleteProjectById(id);
    }
}
