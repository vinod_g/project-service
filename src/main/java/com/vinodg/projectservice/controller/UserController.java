package com.vinodg.projectservice.controller;

import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    public static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getAll() {
        List<UserDTO> users = userService.fetchUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable("id") String id) {
        log.info("Getting User with id {}", id);
        final UserDTO user = userService.fetchUserById(id);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User Not Found");
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") String id, @Valid @RequestBody UserDTO user) {
        user.setId(id);
        userService.updateUser(user);

    }

    @PostMapping()
    public ResponseEntity<UserDTO> create(@Valid @RequestBody UserDTO user) {
        log.info("Creating User : {}", user);
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable String id) {
        log.info("Deleting User with id  {}", id);
        userService.deleteUserById(id);
    }
}
