package com.vinodg.projectservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "projects")
@SuppressWarnings("squid:S1700")
public class Project {

    @Id
    private String id;
    private String project;
    private String startDate;
    private String endDate;
    private Integer priority;
    private Integer managerId;
}