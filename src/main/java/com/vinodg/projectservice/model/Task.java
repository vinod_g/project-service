package com.vinodg.projectservice.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "tasks")
@SuppressWarnings("squid:S1700")
public class Task {

    @Id
    private String id;
    @DBRef
    private ParentTask parent;
    @DBRef
    private Project project;
    private String task;
    private String startDate;
    private String endDate;
    private Integer priority;
}
