package com.vinodg.projectservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "parentTasks")
@SuppressWarnings("squid:S1700")
public class ParentTask {
    @Id
    private String parentId;
    private String parentTask;
}
