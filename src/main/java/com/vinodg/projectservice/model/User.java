package com.vinodg.projectservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "users")
@SuppressWarnings("squid:S1700")
public class User {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private Integer employeeId;
    @DBRef
    private Task task;
    @DBRef
    private Project project;
}