package com.vinodg.projectservice.util;

import com.vinodg.projectservice.dto.ParentTaskDTO;
import com.vinodg.projectservice.dto.ProjectDTO;
import com.vinodg.projectservice.dto.TaskDTO;
import com.vinodg.projectservice.dto.UserDTO;
import com.vinodg.projectservice.model.ParentTask;
import com.vinodg.projectservice.model.Project;
import com.vinodg.projectservice.model.Task;
import com.vinodg.projectservice.model.User;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DTOMapper {

    public UserDTO toUserDTO(User user) {
        if (user == null) {
            return null;
        }
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(), user.getEmployeeId(), toTaskDTO(user.getTask()), toProjectDTO(user.getProject()));
    }

    public TaskDTO toTaskDTO(Task task) {
        if (task == null) {
            return null;
        }
        return new TaskDTO(task.getId(), toParentTaskDTO(task.getParent()), toProjectDTO(task.getProject()), task.getTask(), task.getStartDate(), task.getEndDate(), task.getPriority(), null);
    }

    public ParentTaskDTO toParentTaskDTO(ParentTask parentTask) {
        if (parentTask == null) {
            return null;
        }
        return new ParentTaskDTO(parentTask.getParentId(), parentTask.getParentTask());
    }

    public ProjectDTO toProjectDTO(Project project) {
        if (project == null) {
            return null;
        }
        return new ProjectDTO(project.getId(), project.getProject(), project.getStartDate(), project.getEndDate(), project.getPriority(), 1, 0, 0);
    }

    public User toUser(UserDTO user) {
        if (user == null) {
            return null;
        }
        return new User(user.getId(), user.getFirstName(), user.getLastName(), user.getEmployeeId(), toTask(user.getTask()), toProject(user.getProject()));
    }

    public Task toTask(TaskDTO task) {
        if (task == null) {
            return null;
        }
        return new Task(task.getId(), toParentTask(task.getParent()), toProject(task.getProject()), task.getTask(), task.getStartDate(), task.getEndDate(), task.getPriority());
    }

    public ParentTask toParentTask(ParentTaskDTO parentTask) {
        if (parentTask == null) {
            return null;
        }
        return new ParentTask(parentTask.getParentId(), parentTask.getParentTask());
    }

    public Project toProject(ProjectDTO project) {
        if (project == null) {
            return null;
        }
        return new Project(project.getId(), project.getProject(), project.getStartDate(), project.getEndDate(), project.getPriority(), 1);
    }

    public List<UserDTO> toUserDTOs(List<User> users) {
        if (users == null) {
            return Collections.emptyList();
        }
        return users.stream().map(this::toUserDTO).collect(Collectors.toList());
    }

    public List<ProjectDTO> toProjectDTOs(List<Project> projects) {
        if (projects == null) {
            return Collections.emptyList();
        }
        return projects.stream().map(this::toProjectDTO).collect(Collectors.toList());
    }

    public List<TaskDTO> toTaskDTOs(List<Task> tasks) {
        if (tasks == null) {
            return Collections.emptyList();
        }
        return tasks.stream().map(this::toTaskDTO).collect(Collectors.toList());
    }

    public List<ParentTaskDTO> toParentTaskDTOs(List<ParentTask> parentTasks) {
        if (parentTasks == null) {
            return Collections.emptyList();
        }
        return parentTasks.stream().map(this::toParentTaskDTO).collect(Collectors.toList());
    }

}
