#Project Service

##About
This is the backend service for the project manager application.
Its built using spring boot and with mongo db.

## Steps to run the solution 

### Using Source code
- Start the local mongodb instance
- Run "mvn spring-boot:run"
- The api can be accessed via http://localhost:8090/api/

### Docker
- Use docker-compose up to run the application
- The with docker "docker build -t project-service ."  and run "docker run -d -it -p 8090:8080 --rm --name=ps project-service"
 
